# UA Basic Page Drupal Feature Module

Provides basic page content type.

## Features

- Provides 'ua_page' content type (clone of Basic page content type from Drupal Standard install profile).
